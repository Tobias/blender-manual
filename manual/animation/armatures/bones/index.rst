.. _bpy.types.Bone:
.. _bpy.types.ArmatureBones:

#########
  Bones
#########

.. toctree::
   :maxdepth: 2

   introduction.rst
   bone_collections.rst
   structure.rst
   tools/index.rst
   selecting.rst
   editing/index.rst
   properties/index.rst
