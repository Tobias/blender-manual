.. index:: Geometry Nodes; Duplicate Hair Curves

*********************
Duplicate Hair Curves
*********************

Duplicates hair curves a certain number of times within a radius.

.. peertube:: es5bkTNvRwrvUFdjuK1UvB

Inputs
======

Geometry
   Input Geometry (only curves will be affected).

Amount
   Amount of duplicates per curve.

Viewport Amount
   Percentage of amount used for the viewport.

Radius
   Radius in which the duplicate curves are offset from the guides.

Distribution Shape
   Shape of distribution from center to the edge around the guide.

Tip Roundness
   Offset of the curves to round the tip.

Even Thickness
   Keep an even thickness of the distribution of duplicates.

Seed
   Random Seed for the operation.


Properties
==========

This node has no properties.


Outputs
=======

**Geometry**

Guide Index
   Guide index map that was used for the operation.
